﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace lab9_1
{
    public enum diagnoz { Грип, Ангина, Пневмония }
    public struct bol : IComparable
    {
        public int num;
        public string fam;
        public int year;
        public diagnoz zabolevanie;
        public string days;
        public int d()
        {
            return (DateTime.Now.Date - Convert.ToDateTime(days)).Days;
        }

        public void output()
        {
            Console.WriteLine("|{0,5}|{1,12}|{2,13}|{3,10}|{4,14}|", num, fam, year, days, zabolevanie);
        }

        public int CompareTo(object item)
        {
            bol tmp = (bol)item;
            if (this.d() > tmp.d()) return 1;
            else
                if (this.d() < tmp.d()) return -1;
                else return 0;
        }
    };
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader f = new StreamReader("input.txt");
            string s = f.ReadToEnd();
            f.Close();

            string[] soll = s.Split('\n');
            bol[] seek = new bol[soll.Length];
            int j=0;
            for (int i = 0; i < seek.Length; i++)
            {
               
                string[] temp = soll[i].Split(';');
                if (Enum.IsDefined(typeof(diagnoz), temp[4]))
                {
                    seek[j].zabolevanie = (diagnoz)Enum.Parse(typeof(diagnoz), temp[4]);
                    seek[j].num = Convert.ToInt16(temp[0]);
                    seek[j].fam = temp[1];
                    seek[j].year = Convert.ToInt16(temp[2]);
                    seek[j].days = temp[3];
                    j++;
                }
                else
                {
                    Console.WriteLine(temp[4]);
                    Console.WriteLine("Нет такаой болезни");
                }

                
            
            }
            bol[] seek1 = new bol[j];
            for (int i = 0; i < j; i++)
            {
                seek1[i].num = seek[j].num;
                seek1[i].fam = seek[j].fam;
                seek1[i].year = seek[j].year;
                seek1[i].days = seek[j].days;
                seek1[i].zabolevanie = seek[j].zabolevanie;
            }
            Console.WriteLine("Исходные данные");
            Console.WriteLine("|-----|------------|-------------|----------|--------------|");
            Console.WriteLine("|Номер| Фамилия    | Возраст     |   День   | Болезнь      |");
            Console.WriteLine("|-----|------------|-------------|----------|--------------|");
            for (int i = 0; i < j; i++)
                seek[i].output();
            Console.WriteLine("|-----|------------|-------------|----------|--------------|");

            Console.WriteLine("Для продолжения нажмите любую клавишу");

            ConsoleKeyInfo n = Console.ReadKey(true);
            int nom = 1;
            while (n.Key != ConsoleKey.E)
             {

                Console.Clear();
                Console.WriteLine("Выберите тип болезни");

                if (nom == 1) Console.BackgroundColor = ConsoleColor.Blue;
                Console.WriteLine("1 - Грип ");
                Console.ResetColor();

                if (nom == 2) Console.BackgroundColor = ConsoleColor.Blue;
                Console.WriteLine("2 - Ангина ");
                Console.ResetColor();

                if (nom == 3) Console.BackgroundColor = ConsoleColor.Blue;
                Console.WriteLine("3 - Пневмония ");
                Console.ResetColor();

                if (nom == 4) Console.BackgroundColor = ConsoleColor.Blue;
                Console.WriteLine("4 - Исходные данные");
                Console.ResetColor();

                n = Console.ReadKey();

                while (n.Key != ConsoleKey.Enter)
                {

                    Console.Clear();
                    if (n.KeyChar == '1')
                        nom = 1;
                    else
                        if (n.KeyChar == '2')
                            nom = 2;
                        else
                            if (n.KeyChar == '3')
                                nom = 3;
                            else
                                if (n.KeyChar == '4')
                                    nom = 4;

                    Console.WriteLine("Выберите тип болезни");


                    if (nom == 1) Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("1 - Грип ");
                    Console.ResetColor();

                    if (nom == 2) Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("2 - Ангина ");
                    Console.ResetColor();

                    if (nom == 3) Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("3 - Пневмония ");
                    Console.ResetColor();

                    if (nom == 4) Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("4 - Исходные данные");
                    Console.ResetColor();
                    n = Console.ReadKey();


                    switch (nom)
                    {
                        case 1:
                            {
                                Console.WriteLine("Грип");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                Console.WriteLine("|Номер| Фамилия    | Возраст     |   День   | Болезнь      |");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                for (int i = 0; i < j; i++)
                                    if (string.Compare(Convert.ToString(seek[i].zabolevanie), "Грип") == 0)
                                        seek[i].output();
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");

                            }
                            break;

                        case 2:
                            {
                                Console.WriteLine("Ангина");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                Console.WriteLine("|Номер| Фамилия    | Возраст     |   День   | Болезнь      |");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                for (int i = 0; i < j; i++)
                                    if (string.Compare(Convert.ToString(seek[i].zabolevanie), "Ангина") == 0)
                                        seek[i].output();
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                            }
                            break;

                        case 3:
                            {
                                Console.WriteLine("Пневмония");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                Console.WriteLine("|Номер| Фамилия    | Возраст     |   День   | Болезнь      |");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                for (int i = 0; i < j; i++)
                                    if (string.Compare(Convert.ToString(seek[i].zabolevanie), "Пневмония") == 0)
                                        seek[i].output();
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                            }
                            break;


                        case 4:
                            {
                                Console.WriteLine("Исходные данные");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                Console.WriteLine("|Номер| Фамилия    | Возраст     |   День   | Болезнь      |");
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                                for (int i = 0; i < j; i++)
                                    seek[i].output();
                                Console.WriteLine("|-----|------------|-------------|----------|--------------|");
                            }
                            break;

                        default:
                            break;

                    }
                }

                StreamWriter f1 = new StreamWriter("output.txt");
                string[] g = new string[seek.Length];
                for (int i = 0; i < g.Length; i++)
                {
                    g[i] = seek[i].days;
                }
                Array.Sort(seek);
                string[] names = Enum.GetNames(typeof(diagnoz));
                for(int i=0; i<names.Length; ++i)
                {
                    f1.WriteLine(names[i]);
                    foreach (bol x in seek)
                    {


                        if (string.Compare(names[i], Convert.ToString(x.zabolevanie)) == 0)
                        {
                            f1.WriteLine("|-----|------------|-------------|----------|");
                            f1.WriteLine("|Номер| Фамилия    | Возраст     |   День   |");
                            f1.WriteLine("|-----|------------|-------------|----------|");
                            f1.WriteLine("|{0,5}|{1,12}|{2,13}|{3,10}|", x.num, x.fam, x.year, x.d());
                            f1.WriteLine("|-----|------------|-------------|----------|");
                        }
                        else
                            f1.WriteLine("Нет такой боезни");
                    }
                }
                
                Console.ReadKey();
                f1.Close();
                Console.Clear();
                
            }
        }
    }
}

